﻿
#include <iostream>

void Number(int N, int a)
{
	for (int i = a; i <= N; i += 2)
	std::cout << i << " ";
	std::cout << "\n";
}

int main()
{
	int N;
	std::cin >> N;

	Number(N, 0);
	Number(N, 1);
}